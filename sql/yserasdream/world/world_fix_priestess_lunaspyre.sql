INSERT INTO `creature_template` (entry, minlevel, maxlevel, name, subname, modelid1, rank, type, family) VALUES (118518, '100', '100', 'Sacerdotisa Cimalunar', '', '0', '3', '6', '0') ON DUPLICATE KEY UPDATE minlevel = VALUES(minlevel), maxlevel = VALUES(maxlevel), name = VALUES(name), subname = VALUES(subname), modelid1 = VALUES(modelid1), rank = VALUES(rank), type = VALUES(type), family = VALUES(family);

UPDATE `creature_template` SET `faction` = '14' WHERE `entry` = 118518;

UPDATE `creature_template` SET `mingold` = '0', `maxgold` = '0' WHERE `entry` = 118518;

INSERT INTO `creature_template_locales` (entry, locale, Name, Title) VALUES (118518, 'esES', 'Sacerdotisa Cimalunar', '') ON DUPLICATE KEY UPDATE locale = VALUES(locale), Name = VALUES(Name), Title = VALUES(Title);

UPDATE creature_template SET lootid = 118518 WHERE entry = 118518 AND lootid = 0;
DELETE FROM `creature_loot_template` WHERE `entry` = '118518';
INSERT INTO `creature_loot_template` (`entry`, `Item`, `Reference`, `Chance`, `QuestRequired`, `LootMode`, `GroupId`, `MinCount`, `MaxCount`, `Comment`) VALUES
('118518', '138781', '0', '0.2567931', '0', '1', '0', '1', '1', ''),
('118518', '140221', '0', '0.07166319', '0', '1', '0', '1', '1', ''),
('118518', '140222', '0', '0.2567931', '0', '1', '0', '1', '1', ''),
('118518', '140226', '0', '0.1552702', '0', '1', '0', '1', '1', ''),
('118518', '140227', '0', '0.4717826', '0', '1', '0', '1', '1', ''),
('118518', '140587', '0', '0', '0', '1', '0', '1', '1', ''),
('118518', '144345', '0', '0.2269334', '0', '1', '0', '1', '1', ''),
('118518', '146411', '0', '0.08360705', '0', '1', '0', '1', '1', ''),
('118518', '146413', '0', '0.07763512', '0', '1', '0', '1', '1', ''),
('118518', '0', '118518', '100', '0', '1', '0', '2', '2', ''),
('118518', '147726', '0', '0.08519702', '0', '1', '0', '1', '1', ''),
('118518', '147729', '0', '1.690057', '0', '1', '0', '1', '1', '');


DELETE FROM `reference_loot_template` WHERE `entry` = '118518';
INSERT INTO `reference_loot_template` (`entry`, `Item`, `Reference`, `Chance`, `QuestRequired`, `LootMode`, `GroupId`, `MinCount`, `MaxCount`, `Comment`) VALUES
('118518', '146987', '0', '9.650642', '0', '1', '1', '1', '1', ''),
('118518', '146997', '0', '8.856375', '0', '1', '1', '1', '1', ''),
('118518', '147005', '0', '9', '0', '1', '1', '1', '1', ''),
('118518', '147012', '0', '11', '0', '1', '1', '1', '1', ''),
('118518', '147017', '0', '10.62407', '0', '1', '1', '1', '1', ''),
('118518', '147021', '0', '15.27023', '0', '1', '1', '1', '1', ''),
('118518', '147031', '0', '11.49597', '0', '1', '1', '1', '1', ''),
('118518', '147033', '0', '11', '0', '1', '1', '1', '1', ''),
('118518', '147054', '0', '10.25978', '0', '1', '1', '1', '1', ''),
('118518', '147056', '0', '9.782024', '0', '1', '1', '1', '1', ''),
('118518', '147061', '0', '9.64467', '0', '1', '1', '1', '1', ''),
('118518', '147068', '0', '9.381905', '0', '1', '1', '1', '1', ''),
('118518', '147078', '0', '9.495372', '0', '1', '1', '1', '1', ''),
('118518', '147089', '0', '10.39116', '0', '1', '1', '1', '1', ''),
('118518', '147097', '0', '8.342789', '0', '1', '1', '1', '1', ''),
('118518', '147101', '0', '10.50463', '0', '1', '1', '1', '1', ''),
('118518', '147105', '0', '9.107196', '0', '1', '1', '1', '1', '');


