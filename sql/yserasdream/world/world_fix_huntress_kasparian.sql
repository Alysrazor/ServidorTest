INSERT INTO `creature_template` (entry, minlevel, maxlevel, name, subname, modelid1, rank, type, family) VALUES (118523, '100', '100', 'Cazadora Kasparian', '', '0', '3', '6', '0') ON DUPLICATE KEY UPDATE minlevel = VALUES(minlevel), maxlevel = VALUES(maxlevel), name = VALUES(name), subname = VALUES(subname), modelid1 = VALUES(modelid1), rank = VALUES(rank), type = VALUES(type), family = VALUES(family);

UPDATE `creature_template` SET `faction` = '14' WHERE `entry` = 118523;

UPDATE `creature_template` SET `mingold` = '0', `maxgold` = '0' WHERE `entry` = 118523;

INSERT INTO `creature_template_locales` (entry, locale, Name, Title) VALUES (118523, 'esES', 'Cazadora Kasparian', '') ON DUPLICATE KEY UPDATE locale = VALUES(locale), Name = VALUES(Name), Title = VALUES(Title);

UPDATE creature_template SET lootid = 118523 WHERE entry = 118523 AND lootid = 0;


