/*
* Copyrigh DrustWoW 2019 https://drustwow.com

* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/



#include "AreaTrigger.h"
#include "AreaTriggerAI.h"
#include "ScriptedCreature.h"
#include "ScriptMgr.h"
#include "SpellAuras.h"
#include "InstanceScript.h"
#include "ObjectAccessor.h"
#include "SpellScript.h"
#include "tomb_of_sargeras.h"

//constant positions about teleports and Kasparian Jumps
Position const SistersTeleports[] = // Same positions for Kasparian Jumps
{
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
};

Position const CenterCombat[1] =
{
    {},
};


enum KasparianSays
{

    //Kasparian 
    SAY_KASPARIAN_AGGRO = 1,
    SAY_SPELL_GLAIVE = 2,
    SAY_KASPARIAN_KILLS = 3,
    SAY_KSISTER_KILLS = 4, // When a unactive sister kills a player.
    SAY_KASPARIAN_EVADE = 5,
    SAY_KASPARIAN_DIED = 6,
};

enum YathaeSays
{
    //Yathae
    SAY_YATHAE_AGGRO = 1,
    SAY_PHASE_COMBAT = 2,
    SAY_SPELLS_ARROW = 3,
    SAY_SPELL_AOE = 4,
    SAY_SPELL_PET = 5,
    SAY_YATHAE_KILLS = 6,
    SAY_YSISTER_KILLS = 7,
    SAY_YATHAE_EVADE = 8,
    SAY_YATHAE_DIED = 9,
};

enum LunaspyreSays
{
    //Lunaspyre
    SAY_LUNASPYRE_AGGRO = 1,
    SAY_SPELL = 2,
    SAY_FOUNTAIN_OF_ELUNE = 3,
    SAY_LUNASPYRE_KILLS = 4,
    SAY_LYATHAE_KILLS = 5,
    SAY_LKASPARIAN_KILLS = 6,
    SAY_LUNASPYRE_EVADE = 7,
    SAY_LUNASPYRE_DIED = 8,
};

enum SistersSpells
{
    //Generic
    SPELL_QUIET_SUICIDE = 3617, // Serverside spell
    SPELL_INCORPOREAL_TELEPORT = 236115, //Cast
    SPELL_INCORPOREAL_TELEPORT_2 = 236224, //Not sure if this is the teleport or need script. Instant cast. These two teleport spells are the same for Lunaspyre
    SPELL_COOLDOWN_CREATURE_SPECIAL = 61207, //To block other spells from interfering with their special, for channeling spells I suppose
    SPELL_GHOST_AURA = 235268, //Cosmetic for Sisters
    SPELL_ELUNE_FOUNTAIN = 236357, //Create AT 11284
    SPELL_LUNAR_SUFFUSION = 234995, //Add the aura every 3 seconds on the bright side
    SPELL_UMBRA_SUFFUSION = 234996, //Add the aura every 3 seconds on the dark side
    SPELL_ASTRAL_PURGE = 234998, //Will trigger on change side in Fountain of Elune, and reset Suffusion marks.
    //Generic Talk Not sure about these spells
    //Kasparian
    SPELL_KASPARIAN_KILL_CONVERSATION_A = 243051, //When unactive sister kills a player
    SPELL_KASPARIAN_KILL_CONVERSATION_B = 243052, //When unactive sister kills a player
    SPELL_KASPARIAN_KILL_CONVERSATION_C = 243053, //When unactive sister kills a player
    //Yathae
    SPELL_YATHAE_BECOMES_ACTIVE_CONVERSATION = 243044,  //Probably at Start Combat in Active Phase
    //Lunaspyre
    SPELL_LUNASPYRE_BECOMES_ACTIVE_CONVERSATION = 243047, //Probably at Start Combat in Active Phase
    SPELL_LUNASPYRE_KILL_CONVERSATION_A = 243059, //When unactive sister kills a player
    SPELL_LUNASPYPE_KILL_CONVERSATION_B = 243060, //When unactive sister kills a player
    SPELL_LUNASPYRE_KILL_CONVERSATION_C = 243061, //When unactive sister kills a player
    //Kasparian
    SPELL_TWILIGHT_GLAIVE_AT = 236529,// AT 9785
    SPELL_TWILIGHT_GLAIVE_DAMAGE = 236541,
    SPELL_TWILIGHT_GLAIVE_DUMMY = 237561,
    SPELL_MOON_GLAIVE = 236547,
    SPELL_DISCORPORATE = 236550, //Applied only at the first hit target. Astral Purge will remove it.
    SPELL_GLAIVE_STORM = 239379, //Only used in Elune's Fountain Phase 3 (normal), phase 2 and 3 heroic and mythic
    //Yathae
    SPELL_SHADOW_SHOT = 237630,
    SPELL_INCORPOREAL_SHOT_CAST = 236305,//Only used in Elune's Fountain Phase 2 (normal), phase 1 and 3 heroic and mythic
    SPELL_INCORPOREAL_SHOT_DAMAGE = 236306,
    SPELL_CALL_MOONTALON = 236694,
    SPELL_TWILIGHT_VOLLEY_AT = 236442,// AT 9777
    SPELL_TWILIGHT_VOLLEY_DAMAGE = 236516, //Apply this aura OnUnitEnter();
    SPELL_RAPID_SHOT = 236598,
    SPELL_RAPID_SHOT_AURA = 236596,
    //Lunaspyre
    SPELL_EMBRACE_OF_THE_ECLIPSE_HEAL = 233263,//Shields Heal
    SPELL_EMBRACE_OF_THE_ECLIPSE_BOSS = 233264,//Shield Boss
    SPELL_LUNAR_BEACON = 236712,//OnRemove(); actives Lunar barrage.
    SPELL_LUNAR_FIRE = 239264,//Only on current target,active.
    SPELL_LUNAR_STRIKE = 237632,
    SPELL_MOON_BURN = 236518,//Astral Purge will remove this.
    SPELL_LUNAR_BARRAGE_AT = 236726,//AT 9807
    SPELL_LUNAR_BARRAGE_DAMAGE = 237351,
};

enum GeneralEvents
{
    EVENT_TELEPORT = 1, // Kasparian use EVENT_JUMP instead.
    EVENT_FOUNTAIN_OF_ELUNE, // Spell AreaTrigger
};

enum KasparianEvents
{
    EVENT_MOON_GLAIVE = 1,
    EVENT_TWILIGHT_GLAIVE,
    EVENT_GLAIVE_STORM,
    EVENT_JUMP
};

enum YathaeEvents
{
    EVENT_TWILIGHT_VOLLEY = 1,
    EVENT_RAPID_SHOT,
    EVENT_SHADOW_SHOT,
    EVENT_INCORPOREAL_SHOT,
    EVENT_CALL_MOONTALON
};

enum LunaspyreEvents
{
    EVENT_MOON_BURN = 1,
    EVENT_LUNAR_BEACON,
    EVENT_LUNAR_FIRE,
    EVENT_LUNAR_STRIKE,
    EVENT_EMBRACE_OF_THE_ECLIPSE
};

enum SistersActions
{
    ACTION_AGGRO = 1,
    ACTION_EVADE,
    ACTION_JUST_DIED
};

enum Phases
{
    THE_HUNTRESS = 1,
    THE_CAPTAIN,
    THE_PRIESTESS
};

/*uint32 const SistersData[3] =
{
    //DATA_HUNTRESS_KASPARIAN,
    //DATA_CAPTAIN_YATHAE_MOONSTRIKE,
    //DATA_PRIESTESS_LUNASPYRE
};*/

struct SistersoftheMoonAI : public BossAI
{
    SistersoftheMoonAI(Creature* creature, uint32 bossId) : BossAI(creature, bossId), _bossId(bossId) { }

    Creature* kasparian = instance->GetCreatureEntry(NPC_HUNTRESS_KASPARIAN);
    Creature* yathae    = instance->GetCreatureEntry(NPC_CAPTAIN_YATHAE_MOONSTRIKE);
    Creature* lunaspyre = imstance->GetCreatureEntry(NPC_PRIESTESS_LUNASPYRE);
    
    
    void Reset() override
    {
        me->SetCombatPulseDelay(0);
        events.Reset();
        events.SetPhase(THE_HUNTRESS);

        switch (me->GetEntry())
        {
        case NPC_HUNTRESS_KASPARIAN:
            me->RemoveAurasDueToSpell(SPELL_GHOST_AURA);
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
            break;
        case NPC_CAPTAIN_YATHAE_MOONSTRIKE:
        case NPC_PRIESTESS_LUNASPYRE:
            me->AddAura(SPELL_GHOST_AURA, me);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
            break;
        }
    }

    void EnterCombat(Unit* /*who*/) override
    {
        me->SetCombatPulseDelay(5);
        me->setActive(true);

        DoAction(ACTION_AGGRO);

        switch (me->GetEntry())
        {
        case NPC_HUNTRESS_KASPARIAN:
            me->RemoveUnitMovementFlag(MOVEMENTFLAG_ROOT);
            break;
        case NPC_CAPTAIN_YATHAE_MOONSTRIKE:
        case NPC_PRIESTESS_LUNASPYRE:
            me->SetUnitMovementFlags(MOVEMENTFLAG_ROOT);
            break;
        }

        SisterEvents();
    }

    void DoAction(uint32 action)
    {
        switch (action)
        {
        case ACTION_AGGRO:
            kasparian->AI()->Talk(SAY_KASPARIAN_AGGRO);
            

        }
    }

    virtual void SisterEvents() = 0;

    void JustDied(Unit* killer) override
    {
        if (killer->GetTypeId == TYPEID_PLAYER)
            DoAction(ACTION_JUST_DIED);
    }

    void EnterEvadeMode(EvadeReason why) override
    {

    }

private:
    uint32 _bossId;
};

void AddSC_boss_sisters_of_the_moon()
{

}
