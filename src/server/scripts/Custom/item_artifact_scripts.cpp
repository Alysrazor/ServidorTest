#include "ScriptMgr.h"
#include "GameObject.h"
#include "Item.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "Spell.h"
#include "SpellMgr.h"
#include "TemporarySummon.h"

enum ScytheOfElune
{
    SPELL_SCYTHE_OF_ELUNE = 205387,
    SPELL_NEW_MOON        = 214842,
    SPELL_NEW_MOON_2      = 202767,
    SPELL_PUSH_ACTION_BAR = 225865,
};

class item_scythe_of_elune : public ItemScript
{
public:
    item_scythe_of_elune() : ItemScript("item_scythe_of_elune")  { }
	
	bool OnDummyEffect(Player* player, uint32 newMoon, SpellEffIndex /*effIndex*/, Item* item)
	{
	    uint32 itemId = item->GetEntry();
		newMoon = SPELL_NEW_MOON;
        uint32 newMoonSpell = SPELL_NEW_MOON_2;
        if (player->GetEquippedItem(EQUIPMENT_SLOT_MAINHAND) == item)
        {
            player->AddTemporarySpell(newMoonSpell);
            player->AddAura(SPELL_SCYTHE_OF_ELUNE, player);
            player->AddAura(SPELL_PUSH_ACTION_BAR, player);
        }
        else
        {
            player->RemoveTemporarySpell(newMoonSpell);
            player->RemoveAurasDueToSpell(SPELL_SCYTHE_OF_ELUNE);
            player->RemoveAurasDueToSpell(SPELL_PUSH_ACTION_BAR);
        }
	}
};

void AddSC_item_artifact_scripts()
{
    new item_scythe_of_elune();
}
